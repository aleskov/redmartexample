namespace RpnCalculator
{
    public interface IOperandStack
    {
        double Pop();
    }
}