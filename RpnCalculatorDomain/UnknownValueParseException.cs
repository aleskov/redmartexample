using System;

namespace RpnCalculator
{
    public class UnknownValueParseException : Exception
    {
        public UnknownValueParseException(string value)
        {
            Value = value;
        }

        public string Value { get; }
    }
}