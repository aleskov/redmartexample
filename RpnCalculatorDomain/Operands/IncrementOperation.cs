namespace RpnCalculator.Operands
{
    public class IncrementOperation : IOperation
    {
        private IncrementOperation()
        {
        }

        public static IncrementOperation Instance { get; } = new IncrementOperation();

        public double Calculate(IOperandStack operandProvider)
        {
            var a = operandProvider.Pop();
            return ++a;
        }
    }
}