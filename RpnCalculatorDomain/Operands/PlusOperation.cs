namespace RpnCalculator.Operands
{
    public class PlusOperation : IOperation
    {
        private PlusOperation()
        {
        }

        public static PlusOperation Instance { get; } = new PlusOperation();

        public double Calculate(IOperandStack operandProvider)
        {
            return operandProvider.Pop() + operandProvider.Pop();
        }
    }
}