namespace RpnCalculator.Operands
{
    public interface IOperation
    {
        double Calculate(IOperandStack operandProvider);
    }
}