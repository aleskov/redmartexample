namespace RpnCalculator.Operands
{
    public class MultiplyOperation : IOperation
    {
        private MultiplyOperation()
        {
        }

        public static MultiplyOperation Instance { get; } = new MultiplyOperation();

        public double Calculate(IOperandStack operandProvider)
        {
            return operandProvider.Pop() * operandProvider.Pop();
        }
    }
}