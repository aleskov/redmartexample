namespace RpnCalculator.Operands
{
    public class DivideOperation : IOperation
    {
        private DivideOperation()
        {
        }

        public static DivideOperation Instance { get; } = new DivideOperation();

        public double Calculate(IOperandStack operandProvider)
        {
            var a = operandProvider.Pop();
            var b = operandProvider.Pop();
            return b / a;
        }
    }
}