namespace RpnCalculator.Operands
{
    public class MinusOperation : IOperation
    {
        private MinusOperation()
        {
        }

        public static MinusOperation Instance { get; } = new MinusOperation();

        public double Calculate(IOperandStack operandProvider)
        {
            var a = operandProvider.Pop();
            var b = operandProvider.Pop();
            return b - a;
        }
    }
}