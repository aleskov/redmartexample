namespace RpnCalculator.Operands
{
    public class DecrementOperation : IOperation
    {
        private DecrementOperation()
        {
        }

        public static DecrementOperation Instance { get; } = new DecrementOperation();

        public double Calculate(IOperandStack operandProvider)
        {
            var a = operandProvider.Pop();
            return --a;
        }
    }
}