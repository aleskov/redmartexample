using RpnCalculator.Operands;

namespace RpnCalculator
{
    /// <summary>
    ///     Simplest rpn left-to-right one-thread calculator
    /// </summary>
    public class LeftToRightRpnCalculator
    {
        private readonly OperandStack _operands = new OperandStack();

        public double LastResult { get; private set; }

        private bool IsCalculated()
        {
            return _operands.Values.Count <= 1;
        }
        // for each token in the postfix expression:
        // if token is an operator:
        // operand_2 ← pop from the stack
        // operand_1 ← pop from the stack
        // result ← evaluate token with operand_1 and operand_2
        // push result back onto the stack
        // push token onto the stack
        // else if token is an operand:
        // result ← pop from the stack

        public static double Evaluate(string input)
        {
            var calculator = new LeftToRightRpnCalculator();
            var data = input.Split(' ');
            foreach (var value in data)
                calculator.AddItem(value);

            return calculator.Result();
        }

        public double Result()
        {
            if (!IsCalculated())
                throw new RpnNotCompleteException();

            return LastResult;
        }

        public void AddItem(string item)
        {
            if (!TryAddItem(item))
                throw new UnknownValueParseException(item);
        }

        public bool TryAddItem(string item)
        {
            switch (item)
            {
                case "+":
                    Evaluate(PlusOperation.Instance);
                    break;
                case "-":
                    Evaluate(MinusOperation.Instance);
                    break;
                case "/":
                    Evaluate(DivideOperation.Instance);
                    break;
                case "*":
                    Evaluate(MultiplyOperation.Instance);
                    break;
                case "--":
                    Evaluate(DecrementOperation.Instance);
                    break;
                case "++":
                    Evaluate(IncrementOperation.Instance);
                    break;
                default:
                    if (!int.TryParse(item, out var number)) return false;
                    AddValue(number);
                    break;
            }
            return true;
        }

        public void AddValue(double v)
        {
            LastResult = v;
            _operands.Values.Push(v);
        }

        private void Evaluate(IOperation op)
        {
            LastResult = op.Calculate(_operands);
            AddValue(LastResult);
        }
    }
}