using System.Collections.Generic;

namespace RpnCalculator
{
    internal class OperandStack : IOperandStack
    {
        public readonly Stack<double> Values = new Stack<double>();

        public double Pop()
        {
            return Values.Pop();
        }
    }
}