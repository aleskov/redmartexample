using System.Threading.Tasks;
using Akka.TestKit.Xunit2;
using SpreadsheetProcessing;
using Xunit;

namespace RpnCalculatorTests
{
    public class SheetProcessingTests : TestKit
    {
        [Fact]
        public async Task Formula_with_reference_to_other_cell_value_should_be_processed()
        {
            // Visual look: 
            //    | 1           | 2     | 3            |
            //  --+-------------+-------+--------------+
            //  A | 1 A2 +      | 4     | A1           |
            //  --+-------------+-------+--------------+
            //  B | 1           | 3     | 2            |
            //  ------------------------+--------------+

            var processor = new SpreadsheetProcessor(Sys);
            await processor.Evaluate("3 2",
                    "1 A2 +",
                    "4",
                    "A1",
                    "1",
                    "3",
                    "2")
                .ShouldEqual("3 2",
                    "5.00000",
                    "4.00000",
                    "5.00000",
                    "1.00000",
                    "3.00000",
                    "2.00000");
        }

        [Fact]
        public async Task Output_should_replace_all_operands()
        {
            // Visual look: 
            //    | 1           | 2     | 3            |
            //  --+-------------+-------+--------------+
            //  A |             | 4     | 6            |
            //  --+-------------+-------+--------------+
            //  B | A1          | 3     |              |
            //  ------------------------+--------------+
            var processor = new SpreadsheetProcessor(Sys);
            await processor.Evaluate("3 2",
                    "",
                    "4",
                    "6",
                    "A1",
                    "3",
                    "")
                .ShouldEqual("3 2",
                    "0.00000",
                    "4.00000",
                    "6.00000",
                    "0.00000",
                    "3.00000",
                    "0.00000");
        }

        [Fact]
        public async Task Primitive_Sheet_with_no_rpn_formulas_should_be_processed()
        {
            // Visual look: 
            //    | 1           |
            //  --+-------------+
            //  A | 1           |
            //  --+-------------+


            var processor = new SpreadsheetProcessor(Sys);
            await processor.Evaluate("1 1",
                    "1")
                .ShouldEqual("1 1",
                    "1.00000");
        }

        [Fact]
        public async Task Reference_sheet_should_be_correctly_processed()
        {
            // Visual look: 
            //    | 1           | 2     | 3            |
            //  --+-------------+-------+--------------+
            //  A | A2          | 4 5 * | A1           |
            //  --+-------------+-------+--------------+
            //  B | A1 B2 / 2 + | 3     | 39 B1 B2 * / |
            //  ------------------------+--------------+

            //Input            Expected$Output
            //3 2              3 2
            //A2               20.00000
            //4 5 *            20.00000
            //A1               20.00000
            //A1 B2 / 2 +      8.66667
            //3                3.00000
            //39 B1 B2 * /     1.50000

            var processor = new SpreadsheetProcessor(Sys);
            await processor.Evaluate("3 2",
                    "A2",
                    "4 5 *",
                    "A1",
                    "A1 B2 / 2 +",
                    "3",
                    "39 B1 B2 * /")
                .ShouldEqual("3 2",
                    "20.00000",
                    "20.00000",
                    "20.00000",
                    "8.66667",
                    "3.00000",
                    "1.50000");
        }


        [Fact]
        public async Task Reference_to_other_cell_should_copy_hardcoded_value()
        {
            // Visual look: 
            //    | 1           | 2     | 3            |
            //  --+-------------+-------+--------------+
            //  A | 0           | 4     | A2           |
            //  --+-------------+-------+--------------+
            //  B | 1           | 3     | 2            |
            //  ------------------------+--------------+

            var processor = new SpreadsheetProcessor(Sys);
            await processor.Evaluate("3 2",
                    "0",
                    "4",
                    "A2",
                    "1",
                    "3",
                    "2")
                .ShouldEqual("3 2",
                    "0.00000",
                    "4.00000",
                    "4.00000",
                    "1.00000",
                    "3.00000",
                    "2.00000");
        }

        [Fact]
        public async Task Sheet_with_no_data_should_be_processed()
        {
            // Visual look: 
            //    | 1           | 2     | 3            |
            //  --+-------------+-------+--------------+
            //  A |             |       |              |
            //  --+-------------+-------+--------------+
            //  B |             |       |              |
            //  ------------------------+--------------+

            var processor = new SpreadsheetProcessor(Sys);
            await processor.Evaluate("3 2",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "")
                .ShouldEqual("3 2",
                    "0.00000",
                    "0.00000",
                    "0.00000",
                    "0.00000",
                    "0.00000",
                    "0.00000");
        }

        [Fact]
        public async Task Sheet_with_no_rpn_formulas_should_be_processed()
        {
            // Visual look: 
            //    | 1           | 2     | 3            |
            //  --+-------------+-------+--------------+
            //  A | 1           | 2     | 3            |
            //  --+-------------+-------+--------------+
            //  B | 0           | 0     | 0            |
            //  ------------------------+--------------+
            var processor = new SpreadsheetProcessor(Sys);
            await processor.Evaluate("3 2",
                    "1",
                    "2",
                    "3",
                    "0",
                    "0",
                    "0")
                .ShouldEqual("3 2",
                    "1.00000",
                    "2.00000",
                    "3.00000",
                    "0.00000",
                    "0.00000",
                    "0.00000");
        }

        [Fact]
        public async Task Sheet_with_rpn_formulas_withou_references_should_be_processed()
        {
            // Visual look: 
            //    | 1           | 2     | 3            |
            //  --+-------------+-------+--------------+
            //  A | 1 2 +       | 2     | 3            |
            //  --+-------------+-------+--------------+
            //  B | 0           | 1 5 / | 0            |
            //  ------------------------+--------------+
            var processor = new SpreadsheetProcessor(Sys);
            await processor.Evaluate("3 2",
                    "1 2 +",
                    "2",
                    "3",
                    "0",
                    "1 5 /",
                    "0")
                .ShouldEqual("3 2",
                    "3.00000",
                    "2.00000",
                    "3.00000",
                    "0.00000",
                    "0.20000",
                    "0.00000");
        }
    }
}