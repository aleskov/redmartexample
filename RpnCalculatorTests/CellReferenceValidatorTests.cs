using Should;
using SpreadsheetProcessing;
using Xunit;

namespace RpnCalculatorTests
{
    public class CellReferenceValidatorTests
    {
        [Fact]
        public void Adding_reference_with_complex_cirlce_does_not_pass_validation()
        {
            var validator = new CellReferenceValidator();
            // A1 => A2; A2 => A3; A3 => A1
            var a1 = new CellAddress("A", 1);
            var a2 = new CellAddress("A", 2);
            var a3 = new CellAddress("A", 3);

            validator.Add(a1, a2);
            validator.IsValid.ShouldBeTrue();

            validator.Add(a2, a3);
            validator.IsValid.ShouldBeTrue();

            validator.Add(a3, a1);
            validator.IsValid.ShouldBeFalse();
        }

        [Fact]
        public void Adding_reference_with_self_cirlce_does_not_pass_validation()
        {
            var validator = new CellReferenceValidator();
            // A1 => A1;
            var a1 = new CellAddress("A", 1);

            validator.Add(a1, a1);
            validator.IsValid.ShouldBeFalse();
        }

        [Fact]
        public void Adding_reference_with_simple_cirlce_does_not_pass_validation()
        {
            var validator = new CellReferenceValidator();
            // A1 => A2; A2 => A1
            var a1 = new CellAddress("A", 1);
            var a2 = new CellAddress("A", 2);

            validator.Add(a1, a2);
            validator.IsValid.ShouldBeTrue();

            validator.Add(a2, a1);
            validator.IsValid.ShouldBeFalse();
        }

        [Fact]
        public void Adding_reference_without_cirlce_passes_validation()
        {
            var validator = new CellReferenceValidator();
            // A1 => A2; A2 => A3
            var a1 = new CellAddress("A", 1);
            var a2 = new CellAddress("A", 2);
            var a3 = new CellAddress("A", 3);

            validator.Add(a1, a2);
            validator.IsValid.ShouldBeTrue();

            validator.Add(a2, a3);
            validator.IsValid.ShouldBeTrue();
        }
    }
}