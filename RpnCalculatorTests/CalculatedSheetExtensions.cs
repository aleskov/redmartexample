using System.Linq;
using System.Threading.Tasks;
using Should;
using SpreadsheetProcessing;

namespace RpnCalculatorTests
{
    public static class CalculatedSheetExtensions
    {
        public static async Task ShouldEqual(this Task<CalculatedSheet> sheet, params string[] values)
        {
            (await sheet).ToRawValues().ToArray().ShouldEqual(values);
        }
    }
}