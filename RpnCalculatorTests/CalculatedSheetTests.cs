using Should;
using SpreadsheetProcessing;
using Xunit;

namespace RpnCalculatorTests
{
    public class CalculatedSheetTests
    {
        [Fact]
        public void Format_should_be_dot_and_five_numbers()
        {
            var cell = new CalculatedCell(new CellAddress("A", 1), 5);
            cell.FormattedValue.ShouldEqual("5.00000");
        }
    }
}