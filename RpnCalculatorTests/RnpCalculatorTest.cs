using RpnCalculator;
using Should;
using Xunit;

namespace RpnCalculatorTests
{
    public class RnpCalculatorTest
    {
        [Fact]
        public void Rpn_with_decrement_operator_with_floating_result_should_be_processed()
        {
            LeftToRightRpnCalculator.Evaluate("1 --").ShouldEqual(0);
        }

        [Fact]
        public void Rpn_with_divide_operator_with_floating_result_should_be_processed()
        {
            LeftToRightRpnCalculator.Evaluate("1 3 /").ShouldEqual(0.33333, 4E-6);
        }

        [Fact]
        public void Rpn_with_increment_operator_with_floating_result_should_be_processed()
        {
            LeftToRightRpnCalculator.Evaluate("1 ++").ShouldEqual(2);
        }

        [Fact]
        public void Rpn_with_long_formula_should_be_processed()
        {
            //(2 * 5)--  + (1 / 3) ++
            // = 10 -1 + 0.(3) + 1
            //= 9 + 1.(3) = 10.(3)
            LeftToRightRpnCalculator.Evaluate("2 5 * -- 1 3 / ++ +").ShouldEqual(10.33333, 4E-06);
        }

        [Fact]
        public void Rpn_with_minus_operator_should_be_processed()
        {
            LeftToRightRpnCalculator.Evaluate("3 2 -").ShouldEqual(1);
        }

        [Fact]
        public void Rpn_with_multiple_operator_should_be_processed()
        {
            LeftToRightRpnCalculator.Evaluate("3 2 *").ShouldEqual(6);
        }

        [Fact]
        public void Rpn_with_negative_numbers_should_be_processed()
        {
            LeftToRightRpnCalculator.Evaluate("-1 --").ShouldEqual(-2);
        }


        [Fact]
        public void Rpn_with_plus_operator_should_be_processed()
        {
            LeftToRightRpnCalculator.Evaluate("3 2 +").ShouldEqual(5);
        }

        [Fact]
        public void Rpn_without_operands_should_be_processed()
        {
            LeftToRightRpnCalculator.Evaluate("-1").ShouldEqual(-1);
        }
    }
}