using System.Linq;
using Should;
using SpreadsheetProcessing;
using Xunit;

namespace RpnCalculatorTests
{
    public class CellAddressTests
    {
        [Fact]
        public void CellAddress_range_should_start_with_rows_and_columns_after()
        {
            CellAddress.Range(2, 3)
                .Select(a => a.ToString())
                .ToArray()
                .ShouldEqual(new[] {"A1", "A2", "A3", "B1", "B2", "B3"});
        }

        [Fact]
        public void CellAddress_should_be_comparable()
        {
            var addressA = new CellAddress("A", 1);
            var addressB = new CellAddress("A", 1);

            Assert.True(addressB == addressA);
        }

        [Fact]
        public void CellAddress_should_be_orderable()
        {
            new[] {"A3", "A8", "B1", "B4", "C0", "B6"}
                .Select(a =>
                {
                    CellAddress.TryParse(a, out var address);
                    return address;
                }).OrderBy(a => a)
                .Select(a => a.ToString())
                .ToArray()
                .ShouldEqual(new[] {"A3", "A8", "B1", "B4", "B6", "C0"});
        }
    }
}