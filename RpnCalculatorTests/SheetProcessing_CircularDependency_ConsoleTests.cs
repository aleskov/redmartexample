using RpnCalculatorConsole;
using Should;
using Xunit;

namespace RpnCalculatorTests
{
    public class SheetCircularDependencyTests
    {
        [Fact]
        public void Circular_dependencies_should_exit_with_non_zero_code()
        {
            // Visual look: 
            //    | 1           | 2     | 3            |
            //  --+-------------+-------+--------------+
            //  A | A2          | A3    | B1           |
            //  --+-------------+-------+--------------+
            //  B | B2          | B3    | A1           |
            //  ------------------------+--------------+    

            Program.Main(new[]
            {
                "3 2",
                "A2",
                "A3",
                "B1",
                "B2",
                "B3",
                "A1"
            }).ShouldEqual(1);
        }

        [Fact]
        public void Self_dependencies_in_simple_sheet_should_exit_with_non_zero_code()
        {
            // Visual look: 
            //    | 1           |
            //  --+-------------+
            //  A | A1          |
            //  --+-------------+


            Program.Main(new[]
            {
                "1 1",
                "A1"
            }).ShouldEqual(1);
        }

        [Fact]
        public void Self_dependencies_should_exit_with_non_zero_code()
        {
            // Visual look: 
            //    | 1           | 2     | 3            |
            //  --+-------------+-------+--------------+
            //  A | A1          | A2    | A3           |
            //  --+-------------+-------+--------------+
            //  B | 1           | 3     | 0            |
            //  ------------------------+--------------+    

            Program.Main(new[]
            {
                "3 2",
                "A1",
                "A2",
                "A3",
                "1",
                "3",
                "0"
            }).ShouldEqual(1);
        }

        [Fact]
        public void Simple_reference_dependencies_in_simple_sheet_should_exit_with_non_zero_code()
        {
            // Visual look: 
            //    | 1           | 2           |
            //  --+-------------+-------------+
            //  A | A2          | A1          |
            //  --+-------------+-------------+


            Program.Main(new[]
            {
                "2 1",
                "A2",
                "A1"
            }).ShouldEqual(1);
        }
    }
}