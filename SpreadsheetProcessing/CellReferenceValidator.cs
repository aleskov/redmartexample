using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpreadsheetProcessing
{
    public class CellReferenceValidator
    {
        private readonly Dictionary<CellAddress, Vertex> _knownCellAddresses = new Dictionary<CellAddress, Vertex>();

        public bool IsValid { get; private set; } = true;

        /// <summary>
        ///     returns true if new address were added, false if tried to add known addresses
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public bool Add(CellAddress from, CellAddress to)
        {
            if (!_knownCellAddresses.TryGetValue(from, out var vertexFrom))
            {
                vertexFrom = new Vertex(from.ToString());
                _knownCellAddresses.Add(from, vertexFrom);
            }

            if (!_knownCellAddresses.TryGetValue(to, out var vertexTo))
            {
                vertexTo = new Vertex(from.ToString());
                _knownCellAddresses.Add(to, vertexTo);
            }

            if (vertexFrom.IsConnected(vertexTo))
                return false; //already know the path

            vertexFrom.Connect(vertexTo);

            //if we can travel in back direction, e.g. from "to" to "from" - than we have a circle
            var haveCircle = FindAllPathsBetween(vertexTo, vertexFrom, new Path()).Any();
            IsValid = IsValid && !haveCircle;
            return true;
        }

        private static IEnumerable<Path> FindAllPathsBetween(Vertex current, Vertex end, Path path)
        {
            if (path.Contains(current)) yield break;
            path.Add(current);

            if (current == end) yield return path;

            foreach (var outEdge in current.Connected)
            foreach (var recPath in FindAllPathsBetween(outEdge, end, path.Copy()))
                yield return recPath;
        }

        private class Path
        {
            private List<Vertex> _points = new List<Vertex>();

            public void Add(Vertex e)
            {
                _points.Add(e);
            }

            public Path Copy()
            {
                return new Path {_points = _points.ToList()};
            }

            public bool Contains(Vertex e)
            {
                return _points.Contains(e);
            }

            public override string ToString()
            {
                var sb = new StringBuilder();
                foreach (var point in _points)
                {
                    sb.Append(point.Name);
                    sb.Append(" ");
                }
                return sb.ToString();
            }
        }

        private class Vertex
        {
            private readonly List<Vertex> _connected = new List<Vertex>();

            public Vertex(string name)
            {
                Name = name;
            }

            public string Name { get; }
            public IReadOnlyCollection<Vertex> Connected => _connected;

            public bool IsConnected(Vertex x)
            {
                return _connected.Contains(x);
            }

            public void Connect(params Vertex[] vertices)
            {
                foreach (var e in vertices)
                    AddVertex(this, e);
                //AddVertex(e, this);
            }

            private void AddVertex(Vertex from, Vertex to)
            {
                if (!from._connected.Contains(to))
                    from._connected.Add(to);
            }

            public override string ToString()
            {
                return Name;
            }
        }
    }
}