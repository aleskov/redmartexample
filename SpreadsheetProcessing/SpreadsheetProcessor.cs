using System;
using System.Linq;
using System.Threading.Tasks;
using Akka.Actor;

namespace SpreadsheetProcessing
{
    public class SpreadsheetProcessor
    {
        private readonly IActorRef _sheetActor;
        private CalculatedSheet _calculatedValues;

        public SpreadsheetProcessor(ActorSystem sys)
        {
            _sheetActor = sys.ActorOf<SheetActor>();
        }

        public async Task<CalculatedSheet> Evaluate(params string[] input)
        {
            CreateSheet(input);
            var response = await _sheetActor.Ask<object>(SheetActor.ProcessSheet.Instance);

            switch (response)
            {
                case SheetActor.CalculationFinished result:
                    foreach (var item in result.Cells)
                        _calculatedValues.Add(item.Address, item.Value);
                    return _calculatedValues;
                case Status.Failure failure:
                    throw failure.Cause;
                default:
                    throw new UnknownResponseException();
            }
        }

        private void CreateSheet(string[] input)
        {
            var sheetCreate = ParseSheetCreate(input.First());
            _calculatedValues = new CalculatedSheet(sheetCreate.Rows, sheetCreate.Columns);

            _sheetActor.Tell(sheetCreate);

            var inputIndex = 1;

            foreach (var address in CellAddress.Range(sheetCreate.Rows, sheetCreate.Columns))
            {
                if (inputIndex >= input.Length)
                    throw new CannotFindCellValueInputExcpetion();

                _sheetActor.Tell(new CellDescription(address, input[inputIndex++]));
            }
        }

        //can be parraleled and feed
        private SheetActor.CreateSheet ParseSheetCreate(string input)
        {
            var dimensions = input.Split(' ');
            var columns = int.Parse(dimensions[0]);
            var rows = int.Parse(dimensions[1]);

            return new SheetActor.CreateSheet(rows, columns);
        }

        public class UnknownResponseException : Exception
        {
        }

        public class CannotFindCellValueInputExcpetion : Exception
        {
        }
    }
}