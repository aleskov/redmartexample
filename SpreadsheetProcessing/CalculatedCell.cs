using System.Globalization;

namespace SpreadsheetProcessing
{
    public class CalculatedCell
    {
        private CultureInfo _formatProvider;

        public CalculatedCell(CellAddress address, double value)
        {
            Value = value;
            Address = address;
        }

        public CellAddress Address { get; }

        public double Value { get; }

        public string FormattedValue
        {
            get
            {
                _formatProvider = new CultureInfo("en-US", false);
                return string.Format(_formatProvider, "{0:0.00000}", Value);
            }
        }
    }
}