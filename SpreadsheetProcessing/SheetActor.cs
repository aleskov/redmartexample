using System;
using System.Collections.Generic;
using Akka.Actor;

namespace SpreadsheetProcessing
{
    internal class SheetActor : ReceiveActor
    {
        private readonly CellReferenceValidator _cellReferenceValidator;
        private int _columns;
        private int _rows;

        public SheetActor()
        {
            BecomeStacked(Creating);
            _cellReferenceValidator = new CellReferenceValidator();
        }

        public void Creating()
        {
            var cellsToCreate = 0;
            Receive<CreateSheet>(c =>
                {
                    _rows = c.Rows;
                    _columns = c.Columns;
                    cellsToCreate = _rows * _columns;
                }
            );
            Receive<CellDescription>(c =>
            {
                var cellActor = Context.ActorOf<CellActor>("Cells_" + c.Address);
                cellActor.Tell(c);
                Context.Watch(cellActor);
                cellsToCreate--;
            });

            Receive<ProcessSheet>(c =>
            {
                if (cellsToCreate > 0)
                    throw new SheetNotInitializedException();
                //to avoid storing all refs in-mem
                var selection = Context.ActorSelection("Cells_*");
                selection.Tell(new CellActor.ValueRequest(new CellAddress("", 0), new CellAddress("", 0)));
                BecomeStacked(() => Calculating(Sender));
            });
        }

        public void Calculating(IActorRef client)
        {
            var valuesRemaining = _rows * _columns;
            var calculated = new List<CellActor.ValueCalculated>();
            Receive<CellActor.ValueRequest>(r =>
            {
                _cellReferenceValidator.Add(r.SenderAddress, r.RequestAddress);
                if (!_cellReferenceValidator.IsValid)
                {
                    client.Tell(new Status.Failure(new ReferenceCircularDependencyException()));
                    return;
                }

                Context.ActorSelection("Cells_" + r.RequestAddress)
                    .Tell(r, Sender);
            });
            Receive<Terminated>(t => { client.Tell(t); });
            Receive<Status.Failure>(f => { client.Tell(f); });
            Receive<CellActor.ValueCalculated>(c =>
            {
                calculated.Add(c);
                valuesRemaining--;
                //client.Tell(c);
                if (valuesRemaining == 0)
                {
                    client.Tell(new CalculationFinished(calculated.ToArray()));
                    UnbecomeStacked();
                }
            });
        }

        internal class SheetNotInitializedException : Exception
        {
        }

        internal class CalculationFinished
        {
            public CalculationFinished(CellActor.ValueCalculated[] cells)
            {
                Cells = cells;
            }

            public CellActor.ValueCalculated[] Cells { get; }
        }

        internal class ProcessSheet
        {
            private ProcessSheet()
            {
            }

            public static ProcessSheet Instance { get; } = new ProcessSheet();
        }

        internal class CreateSheet
        {
            public CreateSheet(int rows, int columns)
            {
                Rows = rows;
                Columns = columns;
            }

            public int Rows { get; }
            public int Columns { get; }
        }
    }
}