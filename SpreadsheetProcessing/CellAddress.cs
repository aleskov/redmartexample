using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;

namespace SpreadsheetProcessing
{
    public class CellAddress : IComparable
    {
        private static readonly string AddressPattern = @"(?<row>([A-Z]|[a-z])+)(?<col>(\d)+)";

        public CellAddress(string row, int column)
        {
            Row = row;
            Column = column;
        }

        public int Column { get; }
        public string Row { get; }

        public int CompareTo(object obj)
        {
            if (!(obj is CellAddress address)) throw new InvalidOperationException();

            var rowDiff = string.Compare(Row, address.Row, CultureInfo.InvariantCulture, CompareOptions.IgnoreCase);
            if (rowDiff == 0)
                return Column - address.Column;
            return rowDiff;
        }

        public static IEnumerable<CellAddress> Range(int rowsTotal, int columnsTotal)
        {
            var rowName = 'A';
            var rowNumber = 0;
            while (rowNumber < rowsTotal)
            {
                rowNumber++;

                var columnNumber = 0;
                while (columnNumber < columnsTotal)
                    yield return new CellAddress(rowName.ToString(), ++columnNumber);
                rowName++;
            }
        }

        public override string ToString()
        {
            return Row + Column;
        }

        public static bool operator ==(CellAddress obj1, CellAddress obj2)
        {
            if (ReferenceEquals(obj1, obj2))
                return true;
            if (ReferenceEquals(obj1, null))
                return false;
            if (ReferenceEquals(obj2, null))
                return false;

            return Equals(obj1, obj2);
        }

        // this is second one '!='
        public static bool operator !=(CellAddress obj1, CellAddress obj2)
        {
            return !(obj1 == obj2);
        }


        public override bool Equals(object obj)
        {
            if (obj is CellAddress address)
                return Equals(address);
            return false;
        }

        protected bool Equals(CellAddress other)
        {
            return Column == other.Column && string.Equals(Row, other.Row);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Column * 397) ^ (Row != null ? Row.GetHashCode() : 0);
            }
        }

        // should be Letter + Digit, like A1, C10001
        public static bool TryParse(string address, out CellAddress cellAddress)
        {
            cellAddress = null;
            var matches = Regex.Match(address, AddressPattern);
            if (!matches.Success) return false;

            var columnValue = matches.Groups["col"]?.Value;
            if (!int.TryParse(columnValue, out var column)) return false;

            var row = matches.Groups["row"]?.Value;
            if (string.IsNullOrEmpty(row)) return false;
            cellAddress = new CellAddress(row, column);
            return true;
        }
    }
}