using System.Collections.Generic;
using System.Linq;

namespace SpreadsheetProcessing
{
    public class CalculatedSheet
    {
        private readonly IDictionary<CellAddress, CalculatedCell> _cells = new Dictionary<CellAddress, CalculatedCell>()
            ;

        public CalculatedSheet(int rows, int columns)
        {
            Rows = rows;
            Columns = columns;
        }

        public int Columns { get; }
        public int Rows { get; }

        public void Add(CellAddress addr, double value)
        {
            _cells.Add(addr, new CalculatedCell(addr, value));
        }

        public IEnumerable<string> ToRawValues()
        {
            //real bottleneck! 
            //should order values on-the fly
            yield return $"{Columns} {Rows}";
            foreach (var cell in _cells.Values.OrderBy(c => c.Address).Select(c => c.FormattedValue))
                yield return cell;
        }
    }
}