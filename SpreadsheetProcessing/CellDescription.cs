namespace SpreadsheetProcessing
{
    internal class CellDescription
    {
        public CellDescription(CellAddress address, string value)
        {
            Address = address;
            Value = value;
        }

        public CellAddress Address { get; }
        public string Value { get; }
    }
}