using System;
using System.Collections.Generic;
using System.Linq;
using Akka.Actor;
using RpnCalculator;

namespace SpreadsheetProcessing
{
    internal class CellActor : ReceiveActor, IWithUnboundedStash
    {
        private LeftToRightRpnCalculator _calculator;
        private CellAddress _pendingCellValueResolve;
        private ValueResponse _valueResponseCachedMessage;

        public CellActor()
        {
            BecomeStacked(Initializing);
        }

        private bool IsCalculated { get; set; }
        private CellAddress Address { get; set; }
        private string RawValue { get; set; }
        private Queue<string> UnprocessedFormulaInputs { get; set; } = new Queue<string>();
        private double Value { get; set; }

        public IStash Stash { get; set; }

        private void Initializing()
        {
            Receive<CellDescription>(i =>
            {
                Address = i.Address;
                RawValue = i.Value;
                BecomeStacked(Working);
                Stash.UnstashAll();
            });

            ReceiveAny(o => Stash.Stash());
        }

        private void Working()
        {
            Receive<ValueRequest>(c =>
            {
                if (IsCalculated)
                {
                    Sender.Tell(_valueResponseCachedMessage);
                }
                else
                {
                    IsCalculated = false;
                    _calculator = new LeftToRightRpnCalculator();
                    BecomeStacked(() => Calculating(Sender, c.CallChain));

                    if (!string.IsNullOrEmpty(RawValue))
                        UnprocessedFormulaInputs = new Queue<string>(RawValue.Split(' '));
                    else
                        UnprocessedFormulaInputs.Clear();

                    CalculateFormulaReenterable(c.CallChain);
                }
            });
        }

        private void Calculating(IActorRef client, CellAddress[] callChain)
        {
            Receive<ValueResponse>(c =>
            {
                if (c.Address != _pendingCellValueResolve)
                    throw new ReferenceValueResolutionOutOfOrderException();

                _calculator.AddValue(c.Value);

                CalculateFormulaReenterable(callChain);
            });

            Receive<ValueCalculated>(c =>
            {
                IsCalculated = true;
                Value = c.Value;
                _valueResponseCachedMessage = new ValueResponse(Address, Value);
                client.Tell(c);
                Stash.UnstashAll();
                UnbecomeStacked();
            });

            Receive<ValueRequest>(r => { Stash.Stash(); });

            ReceiveAny(o => Stash.Stash());
        }

        private void CalculateFormulaReenterable(CellAddress[] objCallChain)
        {
            while (UnprocessedFormulaInputs.Count > 0)
            {
                var item = UnprocessedFormulaInputs.Dequeue();
                if (CellAddress.TryParse(item, out var remoteCellAddress))
                {
                    _pendingCellValueResolve = remoteCellAddress;
                    Context.Parent.Tell(new ValueRequest(Address, remoteCellAddress,
                        objCallChain.Concat(new[] {Address}).ToArray()));
                    return;
                }
                _calculator.AddItem(item);
            }

            Self.Tell(new ValueCalculated(Address, _calculator.Result()));
        }

        private class ReferenceValueResolutionOutOfOrderException : Exception
        {
        }

        internal class ValueRequest
        {
            private static readonly CellAddress[] EmptyChain = { };

            public ValueRequest(CellAddress senderAddress, CellAddress requestAddress, params CellAddress[] callChain)
            {
                SenderAddress = senderAddress;
                RequestAddress = requestAddress;
                if (callChain != null && callChain.Any())
                    CallChain = callChain;
            }

            //list of value request initiators, used to detect circular dependencies
            public CellAddress[] CallChain { get; } = EmptyChain;
            public CellAddress SenderAddress { get; }
            public CellAddress RequestAddress { get; }
        }

        internal class ValueResponse
        {
            public ValueResponse(CellAddress address, double value)
            {
                Address = address;
                Value = value;
            }

            public CellAddress Address { get; }
            public double Value { get; }
        }


        internal class ValueCalculated
        {
            public ValueCalculated(CellAddress address, double value)
            {
                Address = address;
                Value = value;
            }

            public CellAddress Address { get; }
            public double Value { get; }
        }
    }
}