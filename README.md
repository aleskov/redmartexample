# RedMartExample

Windows & powershell: 
1) compile project : 
   run 
"dotnet build" 
from project root 

2) go to bin\Debug\netcoreapp2.0

3) create input.txt with data 

4) run 
"dotnet RpnCalculatorConsole.dll (Get-Content input.txt)  | Out-File output.txt"

5) check output


MacOS: 
repeat steps 1) -3)
4) 
  run 
"dotnet RpnCalculatorConsole.dll "1 1" "1" > output.txt  

I had no luck trying to execute:
 "dotnet RpnCalculatorConsole.dll "$(cat input.txt)" > output.txt  
as dotnet interpritate input from cat as a one string instead of many (one per line) 


Considerations and improvements: 

  The main idea of scalable sheet processing is to use actor model and treat each cell as an independent actor, allowing it to calculate each cell value independently. 
It allows to scale out as much as Akka allows. The big deal is not storing the whole sheet in memory anywhere in a program, except results sink, as it needs to order cells by address for correct output. Each cell is created by separate message to sheet actor, results can be gathered by cell too. For simplicity current example gather result in one message, but it is not the best way. For really big sheets we can benefit from Akka streams cells processing. 
  Circular dependencies are resolved as a part of cell values calculation, to remove reference validation step before sheet processing. It is assumed cell dependencies tree can fit into RAM, and it is stored in sheet actor. Runtime cell dependency resolution touches only cells involved in the dependency graph.    

   