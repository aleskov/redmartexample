﻿using System;
using Akka.Actor;
using SpreadsheetProcessing;

namespace RpnCalculatorConsole
{
    public class Program
    {
        public static int Main(string[] args)
        {
            var system = ActorSystem.Create("sheetProcessing");
            var processor = new SpreadsheetProcessor(system);

            try
            {
                var sheet = processor.Evaluate(args).GetAwaiter().GetResult();
                foreach (var res in sheet.ToRawValues())
                    Console.WriteLine(res);
                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return 1;
            }
        }
    }
}